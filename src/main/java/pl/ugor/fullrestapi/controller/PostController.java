package pl.ugor.fullrestapi.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import pl.ugor.fullrestapi.controller.dto.PostDto;
import pl.ugor.fullrestapi.controller.dto.PostDtoMapper;
import pl.ugor.fullrestapi.model.Post;
import pl.ugor.fullrestapi.servis.PostService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;

    private static final String defaultPageNumber = "0";
    private static final String defaultSizeNumber = "20";
    private static final String defaultSortDirection = "ASC";

    @GetMapping("/posts")
    public List<PostDto> getPosts(@RequestParam(defaultValue = defaultPageNumber) int page,
                                  @RequestParam(defaultValue = defaultSizeNumber) int size,
                                  @RequestParam(defaultValue = defaultSortDirection) Sort.Direction sort) {

        int pageNumber = page >= 0 ? page : Integer.parseInt(defaultPageNumber);
        int sizeNumber = size > 0 ? size : Integer.parseInt(defaultSizeNumber);

        return PostDtoMapper.mapToPostDtos(postService.getPosts(pageNumber, sizeNumber, sort));
    }

    @GetMapping("/posts/comments")
    public List<Post> getPostsWithComments(@RequestParam(defaultValue = defaultPageNumber) int page, @RequestParam(defaultValue = defaultSizeNumber) int size) {

        int pageNumber = page >= 0 ? page : 0;
        int sizeNumber = size > 0 ? size : 20;

        return postService.getPostsWithComments(pageNumber, sizeNumber);
    }

    @GetMapping("/posts/{id}")
    public Post getPost(@PathVariable long id) {
        return postService.getSinglePost(id);
    }

    @PostMapping("/posts")
    public Post addPost(@RequestBody Post post){
        return postService.addPost(post);
    }

    @PutMapping("/posts")
    public Post editPost(@RequestBody Post post){
        return postService.editPost(post);
    }

    @DeleteMapping("/posts/{id}")
    public void deletePost(@PathVariable long id){
        postService.deletePost(id);
    }

    // TODO: 2020-08-04 znowu masa zapytan do bazy poleciala deletePost
    // TODO: 2020-08-04 getPostWithCommentsprawdzić na samym zapytaniu jpql lub native Query z joinem w repo
}

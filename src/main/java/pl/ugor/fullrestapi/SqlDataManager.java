package pl.ugor.fullrestapi;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class SqlDataManager {

    // TODO: 2020-07-23 change it for run this method before spring context
    @PostConstruct
    public void sqlDataFileInit() {
        FileWriter fw = null;

        try {
            fw = new FileWriter("src/main/resources/data.sql");
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedWriter bw = new BufferedWriter(fw);
        try {
            for (int i = 1; i <= 50; i++) {
                bw.write("insert into post(id, title, content, created) values (" + i + ", 'test post " + i + "', 'comment " + i + "', '" + LocalDateTime.now().minusMonths(50 - i) + "');");
                bw.newLine();
            }
            for (int i = 1; i <= 50; i++) {
                int postID = 1 + i / 10;
                bw.write("insert into comment(id, post_id, content, created) values (" + i + ", " + postID + ", 'comment " + i + "', '" + LocalDateTime.now().minusHours(50 - i) + "');");
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            bw.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
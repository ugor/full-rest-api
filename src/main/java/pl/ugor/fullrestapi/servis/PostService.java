package pl.ugor.fullrestapi.servis;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.ugor.fullrestapi.exception.PostNotFoundException;
import pl.ugor.fullrestapi.model.Comment;
import pl.ugor.fullrestapi.model.Post;
import pl.ugor.fullrestapi.repository.CommentRepository;
import pl.ugor.fullrestapi.repository.PostRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PostService {

    private final PostRepository postRepository;
    private final CommentRepository commentRepository;

    public List<Post> getPosts(int page, int size, Sort.Direction sort) {
        return postRepository.findAllPosts(
                PageRequest.of(page, size,
                        Sort.by(sort,"created")));
    }

    public Post getSinglePost(long id) {
//        if(postRepository.findById(id).isPresent()){
//            return postRepository.findById(id).get();
//        } else throw new PostNotFoundException(id);
        return postRepository.findById(id).orElseThrow(()->new PostNotFoundException(id));
    }

    public List<Post> getPostsWithComments(int page, int size) {
        List<Post> allPosts = postRepository.findAllPosts(PageRequest.of(page, size));
        List<Long> ids = allPosts.stream()
                .map(post -> post.getId())
                .collect(Collectors.toList());
        List<Comment> comments = commentRepository.findAllByPostIdIn(ids);
        allPosts.forEach(post -> post.setComment(extractComments(comments, post.getId())));
        return allPosts;
    }

    private List<Comment> extractComments(List<Comment> comments, long id) {

        return comments.stream()
                .filter(comment -> comment.getPostId() == id)
                .collect(Collectors.toList());
    }

    public Post addPost(Post post) {
        post.setCreated(LocalDateTime.now());
        return postRepository.save(post);
    }

    @Transactional
    public Post editPost(Post post) {
        Optional <Post> optionalPost = postRepository.findById(post.getId());
        if(optionalPost.isPresent()){
            Post editedPost = optionalPost.get();
            editedPost.setContent(post.getContent());
            editedPost.setTitle(post.getTitle());
            return editedPost;
        } else throw new PostNotFoundException(post.getId());
    }

    public void deletePost(long id) {
        postRepository.findById(id).orElseThrow(()->new PostNotFoundException(id));
        postRepository.deleteById(id);
    }
}

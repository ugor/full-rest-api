package pl.ugor.fullrestapi.servis;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.ugor.fullrestapi.model.Post;
import pl.ugor.fullrestapi.repository.PostRepository;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PostServiceTest {

    @Mock
    private PostRepository postRepository;

    @InjectMocks
    private PostService postService;

    @Test
    void getSinglePost() {

        Post actualPost = new Post();
        actualPost.setId(1);
        actualPost.setTitle("title 1");
        actualPost.setContent("content 1");
        actualPost.setCreated(LocalDateTime.now());
        when(postRepository.findById(1L)).thenReturn(java.util.Optional.of(actualPost));

        Post expectedPost = postService.getSinglePost(1);

        assertEquals(expectedPost.getId(), actualPost.getId());
        assertEquals(expectedPost.getTitle(), actualPost.getTitle());
        assertEquals(expectedPost.getContent(), actualPost.getContent());
        assertEquals(expectedPost.getCreated(), actualPost.getCreated());
        System.out.println(expectedPost.getCreated());
    }
}